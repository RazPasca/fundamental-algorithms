from TreeRepresentations.Transforms import parent_to_multiway_tree
from TreeRepresentations.Trees import MultiwayTree, BinaryTree, ParentTree
"""The algorithms presented bellow run in linear time, as a function of the number of nodes in the tree. Converting from parent
representation to multiway tree is done with the aid of a supplimentary array, which holds the allready created nodes. Shortly, we
parse the array and for each node we try to add itself to its parent vector. If the parents exists, we simply append the new node to
the parent list of children, otherwise we firstly create its parent and only after append it to the children list. All this is done in a
single for loop.

For converting a multiway tree to a binary one, we employ the left child right sibling representation (LCRS). Using this, we reduce the
memory required for storing the data, by removing the memory requirements for storing the array (length, start adress etc). Moreover,
we can apply the operations defined for the binary trees. The algorithm keeps track of the potential brothers of a node( the ones on
the same level) and the previous brothers(on a level higher). Based on this information, we identify several cases:
1) no children and no brothers available->simply create the node and return
2) has children but no brothers->create the node, assign its left brother the call on the first child
3) has brothers but no children-> create the node, assign its right brother the call on the first entry from the brothers list
4) has brothers and children-> create the node, and do 2 and 3 for left and right brothers
"""

def test(parent_vector):
	parent_tree=ParentTree(parent_vector)
	print("parent representation")
	parent_tree.parent_print()
	multiway_tree=MultiwayTree(parent_vector)
	print("multiway representation")
	multiway_tree.print_multi(multiway_tree.root)
	binary_tree=BinaryTree()
	print("binary representation")
	binary_tree.root=binary_tree.multi_to_binary(multiway_tree.root,[])
	binary_tree.print_preorder(binary_tree.root)

def main():
	a=[None,2,7,5,2,7,7,-1,5,2]
	test(a)


if __name__ == '__main__':
	main()