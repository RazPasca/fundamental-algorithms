class ParentRepresentationNode:
	def __init__(self, key, parent=None):
		self.key = key
		if parent == None or key==-1:
			self.parent = None
		else:
			self.parent = parent

	def __repr__(self):
		if self.parent!=None:
			return "val={}, size={}, parent={}".format(self.val,self.parent)

class MultiTreeNode:
	def __init__(self, key):
		self.key = key
		self.children = []

	def __repr__(self):
		return "key= {}".format(self.key)

class BinaryTreeNode:
	def __init__(self, key, left_brother=None, right_brother=None):
		self.key = key
		self.left_brother = left_brother
		self.right_brother = right_brother

	def __repr__(self):
		#if self.left_brother!= None and self.right_brother!=None:
			#return "key={}, left_brother={},right_brother={}".format(self.key,self.left_brother.key,self.right_brother.key)
		return "key={}".format(self.key)