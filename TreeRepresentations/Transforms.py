from TreeRepresentations.NodeClasses import MultiTreeNode, BinaryTreeNode


def parent_to_multiway_tree(parents):
	n= len(parents)-1
	multiway_list=[None for _ in range (0, n+1)]

	root = [None]
	for i in range(1,n+1):
		createNode(parents,i,multiway_list,root)

	return root[0],multiway_list[1:]


def createNode(parent, i, created_nodes, root):
	# If this node is already created
	if created_nodes[i] is not None:
		return

	# Create a new node and update the created_nodes vector
	created_nodes[i] = MultiTreeNode (i)

	if parent[i] == -1: #assign the root if necessary
		root[0] = created_nodes[i]
		return

	# If parent is not created, then create parent first
	if created_nodes[parent[i]] is None:
		createNode (parent, parent[i], created_nodes, root)

	# Find parent pointer and update the children
	p = created_nodes[parent[i]]
	p.children.append(created_nodes[i])

