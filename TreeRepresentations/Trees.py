from conda.cli.main_list import print_packages

from TreeRepresentations.NodeClasses import ParentRepresentationNode, BinaryTreeNode
from TreeRepresentations.Transforms import parent_to_multiway_tree


class ParentTree:
	def __init__(self, node_list):
		self.root = None
		self.create_parent_tree (node_list)
		self.nodes= node_list

	def create_parent_tree(self, node_list):
		for key, parent in enumerate (node_list):
			node = ParentRepresentationNode (key, parent)
			if key == -1:
				self.root = node

	def parent_print(self):
		[print("key={},p={}".format(i,val)) for i,val in enumerate(self.nodes) if i>=1]

class MultiwayTree:
	def __init__(self, node_list):
		self.root, self.nodes = parent_to_multiway_tree (node_list)

	def print_as_list(self):
		for node in self.nodes:
			print (node.key, sep=" ", end=" ")
			if len (node.children) != 0:
				print ("children are", end=" ")
				for child in node.children:
					print (child.key, sep=" ", end=" ")
			print ()

	def print_multi(self,root,spaces=0):
		if root==None:
			return
		printer="  "*spaces
		print(printer,root.key)
		if len(root.children)>=1:
			self.print_multi(root.children[0],spaces+1)
		if len(root.children)>1:
			[self.print_multi(node,spaces+1) for node in root.children[1:]]


class BinaryTree:
	def __init(self):
		self.root = None
		self.calls = 0

	def multi_to_binary(self, node, prev_brothers):
		if len(node.children)>=1:
			next_brother = node.children[0]  # keep the potential brothers
			brothers = node.children[1:]
		elif len(node.children)==1:
			next_brother = node.children[0]  # keep the potential brothers

		new_node = BinaryTreeNode (node.key)
		if len (node.children) == 0 and len (prev_brothers) == 0:
			return new_node

		elif len (node.children) == 0:  # if the current node has no more children, create its right brothers
			new_node.right_brother = self.multi_to_binary (prev_brothers[0], prev_brothers[1:])

		elif len (prev_brothers) == 0:  # if there are no more potential brothers, recurse on the left
			new_node.left_brother = self.multi_to_binary (next_brother, brothers)
		else: #if it has both to the right and to the left, create left and right brothers accordingly
			new_node.left_brother= self.multi_to_binary(next_brother, brothers)
			new_node.right_brother =self.multi_to_binary(prev_brothers[0], prev_brothers[1:])
		return new_node

	def print_preorder(self,root,no_of_spaces=0):
		if root==None:
			return
		printer="  "*(no_of_spaces)
		print(printer,root.key)
		no_of_spaces+=1
		self.print_preorder(root.left_brother,no_of_spaces)
		self.print_preorder(root.right_brother,no_of_spaces-1)

