from DynamicOrderStatistics import DOTree as DOTree
from DynamicOrderStatistics.Node import Node
import random as random
from utilities.generators.listGenerator import sorted_list, uniq_sorted_list
#import os
""" Data order trees (DOT) are augmented BST which are used to perform operations on the nodes depending on their rank. Broadly speaking, they
allow random access of an element stored in them. This is to be done without affecting the previous operations runtimes. Moreover, since we
use an additional field for keeping the size attribute, we also have to preserve this property and update it without affecting the old runtimes.
We do this by modifying the find min procedure and the search procedure by adding some constant time comparisons or assignments. By the master
theorem, the runtime remains O(tree.height). Since we build the tree from an inorder array, we end up with a perfectly balanced tree. This means
we have a height of log(n), therefore all our procedures run in O(logn). For our specific operations, we have the following:
1. Search(rank)- worst case is logn, when we recurse all the way down to the leaf (e.g: first or last element).
2. Delete(rank)- worst case is still logn. Firstly we search the node, and after that we look for the succesor of that node in its right
subtree. We get therefore 2 calls, each with its own height. The 2 added heights= log(n)."""

def testBST(a, f):
	tree = DOTree.DOTree()
	tree.create (a)
	for i in range (1, len (a) + 1):
		tree.search (random.randint (1, len (a)-i+1))
		tree.delete (random.randint (1, len (a)-i+1))
	#tree.traverse ()
	#print ("{0},{1},{2}".format (len (a), tree.searchOps // len (a), tree.deleteOps // len (a)))
	f.write ("{0},{1},{2}\n".format (len (a), tree.searchOps // len (a), tree.deleteOps // len (a)))

def test():
	tree = DOTree.DOTree ()
	a= range(1,8)
	tree.create (a)
	tree.traverse()
	print("We will delete", tree.search(3))
	tree.delete(3)
	tree.traverse()
	print("We will delete", tree.search(3))
	tree.delete(3)
	tree.traverse()
	print("We will delete", tree.search(3))
	tree.delete(3)
	tree.traverse()
	print("We will delete", tree.search(3))
	tree.delete(3)
	tree.traverse()
	print("We will delete", tree.search(3))
	tree.delete(3)
	tree.traverse()
	print("We will delete", tree.search(2))
	tree.delete(2)
	tree.traverse()
	print("We will delete", tree.search(1))
	tree.delete(1)

	tree.traverse()
	# for i in range (1, len (a) + 1):
	# 	index = 3 % tree.root.size
	# 	print("we searched",tree.search(index))
	# 	tree.delete(index)
	# 	tree.traverse()

	# tree.traverse()
	# tree.delete(3)
	# tree.traverse()
	# tree.delete(1)
	# tree.traverse()


def main():
	try:
		f = open ("operations1.csv", "w")
	except FileNotFoundError:
		print("file not opened")
		return
	f.write("n,search ops, delete ops\n")
	for i in range (100, 10001, 100):
		for _ in range(0,5):
			a = range (1, i + 1)
			testBST (a, f)
	f.close ()

if __name__ == '__main__':
	#main ()
	test()

