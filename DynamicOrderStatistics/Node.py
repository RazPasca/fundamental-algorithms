class Node:
	def __init__(self, val):
		self.val = val
		self.right = None
		self.left = None
		self.size = 0
		self.parent = None

	def __init__(self, val, size):
		self.val = val
		self.right = None
		self.left = None
		self.parent = None
		self.size = size

	def __repr__(self):
		if self.parent!=None:
			return "val={}, size={}, parent={}".format(self.val,self.size,self.parent.val)
		return "val={}, size={}".format (self.val, self.size)

	def getLeftSize(self):
		if self.left == None:
			return 0
		return self.left.size