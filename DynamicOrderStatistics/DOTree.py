from astropy.table import sorted_array

from DynamicOrderStatistics.Node import Node


class DOTree:
	def __init__(self, node=None):
		self.root = node
		self.searchOps=0
		self.deleteOps=0

	def search(self, i, node=None):
		if node == None:
			node = self.root

		self.searchOps+=1
		before = node.getLeftSize() + 1  # rank of root in the subtree rooted at itself
		self.searchOps+=1

		if i == before:
			return node

		elif i < before:
			self.searchOps+=1
			return self.search (i,node.left)
		else:
			self.searchOps+=2
			return self.search (i - before,node.right)

	def search_for_delete(self,rank, node=None):
		if node == None:
			node = self.root

		self.deleteOps+=1
		before = node.getLeftSize() + 1  # rank of root in the subtree rooted at itself
		node.size -= 1

		self.deleteOps+=1
		if rank == before:
			#node.size-=1
			return node

		elif rank < before:
			self.deleteOps+=1
			#node.size-= 1
			return self.search_for_delete (rank,node.left)
		else:
			self.deleteOps+=2
			#node.size-=1
			return self.search_for_delete (rank - before,node.right)

	def create(self, num):
		self.root = self.sortedArrayToBST (num,None)

	def sortedArrayToBST(self, num, parent):
		if len (num) == 0: return None
		if len (num) == 1:
			root = Node (num[0], 1)
			root.parent= parent
			return root

		mid = int (len (num) / 2)
		root = Node (num[mid], len (num))
		root.parent = parent
		num1 = num[0:mid]
		num2 = num[(mid + 1):]
		root.left = self.sortedArrayToBST (num1,root)
		root.right = self.sortedArrayToBST (num2,root)
		return root

	def inorder(self, root):
		if root != None:
			self.inorder (root.left)
			print (root)
			self.inorder (root.right)

	def traverse(self,root=None):
		if root==None:
			root=self.root
		currentLevel = [root]
		while currentLevel:
			nextLevel = list ()
			for n in currentLevel:
				if n!= None:
					print("{},size= {} ".format(n.val, n.size),end=" ")
					if n.left: nextLevel.append (n.left)
					if n.right: nextLevel.append (n.right)
			print(" ")
			currentLevel = nextLevel

	def find_min(self,root):
		while root.left!=None:
			root.size-=1
			root=root.left
			self.deleteOps+=2
		return root

	def delete(self,rank,node=None):
		if node==None:
			node=self.root
		to_delete=self.search_for_delete(rank,node)
		#print("we will delete",to_delete)

		self.deleteOps+=3
		if to_delete.left== None or to_delete.right==None:
			y=to_delete
		else:
			y=self.find_min(to_delete.right)

		self.deleteOps+=2
		if y.left != None:
			x= y.left
		else:
			x= y.right

		self.deleteOps+=1
		if x!= None: # y is not a leaf
			self.deleteOps+=1
			x.parent = y.parent

		self.deleteOps+=1
		if y.parent == None: # empty tree
			self.root=x
			self.deleteOps+=1
		elif y== y.parent.left:
			self.deleteOps+=2
			y.parent.left=x
		else:
			self.deleteOps+=3
			y.parent.right = x

		self.deleteOps+=1
		to_delete.val = y.val

		del y
