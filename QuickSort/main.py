import sys

from HeapSort.Heap.Heapsort import heap_sort1
from QuickSort.quickSort import quick_sort, _quick_sort, _quick_sort2
from utilities.generators.listGenerator import avg_list, sorted_list, descending_list
"""Out of the 2 algorithms, basic implementations taken into account, only HeapSort is optimal(O(nlogn)), since QuickSort is running in O(n^2).
However, the multiplicative constant of HeapSort is large, so a good implementation of QuickSort beats it in practice. By good, we aim to avoid
the worst case (since average case is almost the same performance as best case) or always enter best case. In this implementation, we always enter
bestcase by ensuring that our array is always partitioned in equal parts. This is done with the quickSelect algorithm, which returns the k'th
smallest element of an array (for our case, the median). However, quickselect also has a worstcase of O(n^2) (since it inherits quicksort
strategy, but with only 1 recursive call), which needs to be avoided too. This is done by choosing randomly the pivot for our algorithm.
Therefore, complexity it is reduced to O(n) time for finding the median. Thus, by the master theorem, b=2 for the quicksort calls on the 2 parts,
which maximizes its efficiency to O(nlogn) always. Talking about the worst case, for quicksort it happens when the 2 partitions are as unbalanced
as possible, which means that we always pick either the lowest or the highest number as pivot. For instance, sorting an already sorted array
with quicksort having the pivot the leftmost element leads to this terrible complexity. The 2 alogrithms also resemble by looking at memory
required, both being in-place sorting algorithms."""

def main1():

	a = [avg_list(10000) for _ in range (0, 6)]
	f = open ("avgcase1.csv", "w")
	f.write("input size, heapsort ops, quicksort ops\n")
	x=5
	for i in range (100,10001,100):
		vf1=0
		vf2=0
		for j in range(0,6):
			v1=_quick_sort2(a[j][:i])
			b=a[j][:i]
			v2=heap_sort1(b)
			vf1+=v1
			vf2+=v2
		vf1=vf1//5
		vf2=vf2//5
		f.write("{0},{1},{2}\n".format(i,vf2,vf1))


def main2():
	sys.setrecursionlimit (3000)
	f1=open("worstcase.csv","w")
	f1.write("input size, operations\n")
	a=sorted_list(10000)
	for i in range (100,10001,100):
		v1=_quick_sort2(a[:i])
		f1.write("{0},{1}\n".format(i,v1))


def main3():
	f1 = open ("bestcase.csv", "w")
	f1.write ("input size, operations\n")
	a = avg_list (10000)
	for i in range (100, 10001, 100):
		v1 = _quick_sort (a[:i])
		f1.write ("{0},{1}\n".format (i, v1))


if __name__ == '__main__':
	main2()



