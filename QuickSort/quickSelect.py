from random import randint


def partition(a, lo, hi):
	#pivot = lo
	ops=0
	rand=randint(lo,hi)
	ops+=3
	a[lo],a[rand]=a[rand],a[lo]
	pivot=lo
	left = lo+1
	right = hi

	while True:
		ops += 1
		while left < hi and a[left] <= a[pivot]:
			left += 1
		ops += 1
		while right > lo and a[right] >= a[pivot]:
			right -= 1
		if left >= right:
			break
		else:
			ops += 3
			a[left], a[right] = a[right], a[left]
	ops += 3
	a[pivot], a[right] = a[right], a[pivot]
	# print(a)
	return right, ops


def quickSelect(a, lo, hi, k, ops=0):
	if lo == hi:
		return lo, ops
	pos, ops = partition (a, lo, hi)
	numbers = pos - lo + 1
	if numbers == k:
		return pos, ops
	if numbers > k:
		return quickSelect (a, lo, pos - 1, k, ops)
	else:
		return quickSelect (a, pos + 1, hi, k - numbers)
