from QuickSort.quickSelect import partition, quickSelect
from utilities.Decorator import timeit


def quick_sort(a, lo, hi,ops=0):
	if lo < hi:
		p, ops = quickSelect (a, lo, hi, (hi-lo+1)//2)
		ops += quick_sort (a, lo, p - 1,ops)
		ops += quick_sort (a, p + 1, hi,ops)
	return ops


def _quick_sort(a):
	return (quick_sort (a, 0, len (a) - 1))


def _quick_sort2(a):
	return (quick_sort2 (a, 0, len (a) - 1))


def quick_sort2(a, lo, hi):
	ops=0
	if lo < hi:
		pivot, ops = partition2 (a, lo, hi,ops)
		ops += quick_sort2 (a, lo, pivot - 1)
		ops += quick_sort2 (a, pivot + 1, hi)
	return ops


def partition2(a, lo, hi,ops=0):
	pivot = lo
	left = lo + 1
	right = hi

	while True:
		while left < hi and a[left] <= a[pivot]:
			ops += 1
			left += 1

		while right > lo and a[right] >= a[pivot]:
			ops += 1
			right -= 1

		if left >= right:
			break
		else:
			ops += 3
			a[left], a[right] = a[right], a[left]
	ops += 3
	a[pivot], a[right] = a[right], a[pivot]
	return right, ops



