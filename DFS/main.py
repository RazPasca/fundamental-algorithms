import random

from DFS.graph import Graph
"""DFS is an algorithm used to discover all the vertices in a graph connected to the one from which the search starts. It
 employs as a strategy a "jump" to the next available vertex(the neighbor of the current node) and redo the same routine
 until there are no more alternatives or the edge has beend already visited. In contrast to BFS, the boundary is a in depth one,
 dividing the graph in 2 regions: to the left and to the right, instead of above and below such as in BFS. The grey nodes are the
 boundary. The runtime is the same as BFS -> O(V+E), given an adj list representation. Each vertex is initialised before the call
 and is updated along the traversal, together with its neighbors. Each edge is processed once, since we traverse each adj list once,
  which results in O(V+E) as stated above. The main advantage of DFS over BFS is that it requires less memory (no need to store
  child pointers at each level). Also, when searching for solutions in an infite-long(or very long search space), DFS might never
  return with a solution, since the branch taken might be a wrong one, whilst BFS will explore all the possible alternatives (braches)
  in parrallel and come back with a solution.

  Topological sorting employs a simple DFS traversal on the graph and printing the vertices in reverse order of their finish times.
  This results in a O(E+V) complexity when using an auxiliary stack for storing the vertices on which the recursive call was done. It
  also has lots of applications, but works only on acyclic graphs, otherwise we run into a infinite loop when checking the order
  of executing tasks.

  Tarjan algorithm also employs DFS skeleton. We keep track for 2 properties for the nodes: the first time being discovered in DFS,
  and order of the ancestor by who it was discovered(can lead to back edge). For any vertex, if both are the same, it means that the
  vertex is not part of a cycle, or it is part of a cycle since we were able to come back from where we started through a cycle.
  In either case, we print the ancestors and we end up with the strongly connected component """

#PS: I had no time to optimize the addition of edges in graph or checking if a vertex was already visited(got a dictionary size
# changed during iteration) and didnt have time to fix it due to the contest :(
# : for dense graphs we add all the edges and randomly choose which to
#remove
def proof_topological():
	g = Graph (6)
	g.add_edge (5, 2)
	g.add_edge (5, 0)
	g.add_edge (4, 0)
	g.add_edge (4, 1)
	g.add_edge (2, 3)
	g.add_edge (3, 1)
	g.add_edge (3, 5)
	# g.print_adj_list()
	#g.DFS_all ()
	# g.print_adj_list()
	#g.topological_sort ()
	#print (g.acyclic)

def proof_Tarjan():
	g1 = Graph (5)
	g1.add_edge (1, 0)
	g1.add_edge (0, 2)
	g1.add_edge (2, 1)
	g1.add_edge (0, 3)
	g1.add_edge (3, 4)
	g1.Tarjan_All()


def test_E_variation(V, E, f):
	g = Graph (V)
	edges_added = 0
	while edges_added < E:
		start = random.randrange (0, V)
		end = random.randrange (0, V)
		if g.add_edge (start, end):
			edges_added += 1
	g.DFS_all ()
	# print(len(g.node_list))
	# print(edges_added)

	f.write ("{},{},{}\n".format (V, E, g.ops))


def test_V_variation(V, E, f):
	g = Graph (V)
	edges_added = 0
	while edges_added < E:
		start = random.randrange (0, V)
		end = random.randrange (0, V)
		if g.add_edge (start, end):
			edges_added += 1
	g.DFS_all ()
	# print(len(g.node_list))
	# print (edges_added)
	f.write ("{},{},{}\n".format (V, E, g.ops))


def main():
	f = open ("E variantion.csv", "w")
	f.write ("V,E,ops\n")
	for E in range (1000, 5001, 100):
		test_E_variation (100, E, f)

	f1 = open ("V variation.csv", "w")
	f1.write ("V,E,ops\n")
	for V in range (100, 201, 10):
		test_V_variation (V, 9000, f1)


if __name__ == '__main__':
	#proof_topological ()
	#proof_Tarjan()
	main()
