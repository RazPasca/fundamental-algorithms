import sys
from collections import deque

from TreeRepresentations.NodeClasses import MultiTreeNode


class Node:
	def __init__(self, key):
		self.key = key
		self.color = "white"
		self.neighbors = {}
		self.discovery_time = 0
		self.finish_time = 0

	def addNeighbor(self, nbr):
		self.neighbors[nbr] = 1

	def __repr__(self):
		if self != None:
			return "key={},discovery time={},finish time={}".format (self.key, self.discovery_time, self.finish_time)


class Graph:
	def __init__(self, V):
		self.V = V
		self.time = 0
		self.ops = 0
		self.node_list = {}
		self.acyclic=True
		self.unvisited_node_list = {}  # keep track of visited nodes in order to improve running time when checking for potential start nodes
		for i in range (V):
			self.add_node (i)

	def add_node(self, key):
		new_node = Node (key)
		self.node_list[key] = new_node
		self.unvisited_node_list[key] = new_node
		self.ops+=2 #count the operations which initialize the node parameters

	def add_edge(self, start, end):
		if self.node_list[end] not in self.node_list[start].neighbors and end!=start: #if the edge doesn't exist, we add it
			self.node_list[start].addNeighbor (self.node_list[end])
			return True
		else:
			return False #else we return false


	def print_adj_list(self):
		for _,node in self.node_list.items():
			print("for node {} we have the neighbors:".format(node.key))
			for key,nbr in node.neighbors.items():
				print(key)

	def topological_sort(self):
		"""to do: check if acyclic, can use back_edge finding function (for u and v, d[v]<d[u]<f[u]<f[v])"""
		if self.acyclic == False:
			print("the graph has no topological sort")
		else:
			print("one topological ordering of the graph is:")
			nodes=list(self.node_list.values())
			nodes.sort(key=lambda node:node.finish_time,reverse=True)
			[print(node) for node in nodes]

	def DFS_all(self):
		for node in list(self.node_list.values()):
			self.ops += 1
			if node.color == "white":
				self.DFS (node)


	def DFS(self, start_node):
		start_node.color = "grey"
		start_node.discovery_time = self.time
		self.time += 1
		self.ops += 2  # color,discovery time
		for nbr in start_node.neighbors:
			self.ops += 1
			if nbr.color == "grey":
				self.acyclic= False
			self.ops += 1
			if nbr.color == "white":
				self.DFS (nbr)
		self.time += 1
		start_node.color = "black"
		start_node.finish_time = self.time
		self.ops += 2
		del self.unvisited_node_list[start_node.key]


	def Tarjan(self, key, lowest_found, discovery_times, nodes_on_stack, stack):

		discovery_times[key] = self.time
		lowest_found[key] = self.time
		self.time += 1
		nodes_on_stack[key] = True
		stack.append (key)
		for nbr in self.node_list[key].neighbors:
			# If v is not visited yet, then recur for it
			if discovery_times[nbr.key] == -1:
				self.Tarjan (nbr.key, lowest_found,discovery_times,nodes_on_stack,stack)
				lowest_found[key] = min(lowest_found[key], lowest_found[nbr.key])
			elif nodes_on_stack[nbr.key]==True:
				lowest_found[key]=min(lowest_found[key], discovery_times[nbr.key])
		head=-1
		if lowest_found[key] == discovery_times[key]:
			while head!=key:
				head= stack.pop()
				print(head,end=" ")
				nodes_on_stack[head]=False
			print()

	def Tarjan_All(self):
		"""I use a different structure since it doesn't employ the previous DFS used function, because it affects the
		existing data obtained from the DFS traversal which is then used for topological sort. Also, it would make it
		too difficult to read and understand, by combining 3 function calls in a single one"""
		self.time=0
		discovery_times=[-1]*self.V
		lowest_found=[-1]*self.V
		nodes_on_stack=[False]*self.V
		stack=[]

		for key in range(self.V):
			if discovery_times[key]==-1:
				self.Tarjan(key,lowest_found,discovery_times,nodes_on_stack,stack)



