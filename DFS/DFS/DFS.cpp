#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//#include "Profiler.h"

using namespace std;

int time = 0;
//Profiler profiler("demo");

typedef struct nodeG nodeG;

typedef struct nodeLista
{
	nodeG *node;
	struct nodeLista *next;
}nodeLista;

typedef struct lista //folosita pentru lista de adiacenta a fiecarui nod
{
	nodeLista *first;
	nodeLista *last;
	int size;
}lista;

typedef struct queue// coada in care punem nodurile care sunt in curs de vizitare
{
	nodeLista *head;
	nodeLista *tail;
	int size;
}queue;

typedef struct nodeG
{
	int key;
	int start_time;
	int finish_time;
	int color;
	int dist;
	lista *vecini;
}nodeG;

lista* createList()
{
	lista *list = (lista*)malloc(sizeof(lista));
	list->first = NULL;
	list->last = NULL;
	list->size = 0;
	return list;
}

nodeG* createNodeG(int key)
{
	nodeG *node = (nodeG*)malloc(sizeof(nodeG));
	node->key = key;
	node->vecini = createList();
	node->color = 0;
	node->start_time = 0;
	node->finish_time = 0;
	node->dist = 0;
	return node;
}

nodeLista* createGraphNodeList(nodeG *node)
{
	nodeLista *item = (nodeLista*)malloc(sizeof(nodeLista));
	item->node = node;
	item->next = NULL;
	return item;
}

void addNodeL(lista* list, nodeLista *node)
{
	if (list->size == 0)
	{
		list->first = node;
		list->last = node;
		list->size = 1;
	}
	else {
		list->last->next = node;
		list->last = node;
		list->size++;
	}
}

queue* createQueue()
{
	queue *q = (queue*)malloc(sizeof(queue));
	q->head = NULL;
	q->tail = NULL;
	q->size = 0;
	return q;
}

nodeLista* createQueueNode(nodeG *node)
{
	nodeLista *elem = (nodeLista*)malloc(sizeof(nodeLista));
	elem->node = node;
	elem->next = NULL;
	return elem;
}

void enqueue(queue* q, nodeLista* element)
{
	if (q->size == 0)
	{
		q->head = element;
		q->tail = q->head;
		q->size = 1;
	}
	else {
		q->tail->next = element;
		q->tail = element;
		q->size++;
	}
}

nodeLista* dequeue(queue* q)
{
	nodeLista *node;
	if (q->size == 1)
	{
		node = q->head;
		q->head = NULL;
		q->tail = NULL;
		q->size = 0;
	}
	else {
		node = q->head;
		q->head = q->head->next;
		q->size--;
	}
	return node;
}

nodeG** createGraphNodes(int marime)
{
	nodeG **nodes = (nodeG**)malloc(sizeof(nodeG*)*marime);
	for (int i = 0; i<marime; i++)
		nodes[i] = createNodeG(i);
	return nodes;
}

void randomGraphGenerator(nodeG **arr, int verticeNo, int edgeNo)
{
	int i, x, y;
	nodeLista *iterator;
	i = 0;
	while (i<edgeNo)
	{
		do {
			x = rand() % verticeNo;
			y = rand() % verticeNo;
		} while (x == y);
		if (x<y)
			swap(x, y);
		nodeLista *node1 = createGraphNodeList(arr[x]);
		nodeLista *node2 = createGraphNodeList(arr[y]);
		addNodeL(arr[y]->vecini, node1);
		addNodeL(arr[x]->vecini, node2);
		i++;
	}
	for (i = 0; i<verticeNo - 1; i++)
	{
		if (arr[i]->vecini != NULL && arr[i]->vecini->first != NULL)
		{
			iterator = arr[i]->vecini->first;
			while (iterator != NULL)
			{
				printf("%d", iterator->node->key);
				iterator = iterator->next;
			}
			printf("\n");
		}
	}
}


void BFS(nodeG **arr, nodeG *source, int v, int e, char mode)
{
	source->color = 1;
	source->dist = 0;

//	if (mode == 'e')
	///	profiler.countOperation("Edge Operations", e, 3);
	//else
	//	profiler.countOperation("Node Operations", v, 3);

	queue *q = createQueue();
	enqueue(q, createQueueNode(source));

	while (q->tail != NULL)
	{
		nodeG *node = dequeue(q)->node;
		nodeLista *vecini = node->vecini->first;
		while (vecini != NULL)
		{
			if (mode == 'e')
				profiler.countOperation("Edge Operations", e, 1);
			else
				profiler.countOperation("Node Operations", v, 1);
			if (vecini->node->color == 0)
			{
				vecini->node->color = 1;
				vecini->node->parent = node;
				vecini->node->dist = node->dist + 1;
				enqueue(q, createQueueNode(vecini->node));
				if (mode == 'e')
					profiler.countOperation("Edge Operations", e, 3);
				else
					profiler.countOperation("Node Operations", v, 3);
			}
			vecini = vecini->next;
		}
		node->color = 2;
		if (mode == 'e')
			profiler.countOperation("Edge Operations", e, 1);
		else
			profiler.countOperation("Node Operations", v, 1);
		printf("%d ", node->key);

	}

}

int main() {

	int i, v, e;
	nodeLista *iterator;

	//demo
	nodeG **demo;
	demo = createGraphNodes(7);

	nodeLista *node1 = createGraphNodeList(demo[1]);
	nodeLista *node2 = createGraphNodeList(demo[2]);
	addNodeL(demo[2]->vecini, node1);
	addNodeL(demo[1]->vecini, node2);

	nodeLista *node3 = createGraphNodeList(demo[1]);
	nodeLista *node4 = createGraphNodeList(demo[3]);
	addNodeL(demo[3]->vecini, node3);
	addNodeL(demo[1]->vecini, node4);

	nodeLista *node5 = createGraphNodeList(demo[0]);
	nodeLista *node6 = createGraphNodeList(demo[2]);
	addNodeL(demo[2]->vecini, node5);
	addNodeL(demo[0]->vecini, node6);

	nodeLista *node7 = createGraphNodeList(demo[3]);
	nodeLista *node8 = createGraphNodeList(demo[4]);
	addNodeL(demo[4]->vecini, node7);
	addNodeL(demo[3]->vecini, node8);

	nodeLista *node9 = createGraphNodeList(demo[4]);
	nodeLista *node10 = createGraphNodeList(demo[2]);
	addNodeL(demo[2]->vecini, node9);
	addNodeL(demo[4]->vecini, node10);

	nodeLista *node11 = createGraphNodeList(demo[0]);
	nodeLista *node12 = createGraphNodeList(demo[3]);
	addNodeL(demo[3]->vecini, node11);
	addNodeL(demo[0]->vecini, node12);

	nodeLista *node13 = createGraphNodeList(demo[5]);
	nodeLista *node14 = createGraphNodeList(demo[6]);
	addNodeL(demo[2]->vecini, node13);
	addNodeL(demo[3]->vecini, node14);

	printf("\nListe de adiacenta:\n");
	for (i = 0; i<7; i++)
	{
		if (demo[i]->vecini != NULL && demo[i]->vecini->first != NULL)
		{
			printf("Nodul %d ", i);
			iterator = demo[i]->vecini->first;
			while (iterator != NULL)
			{
				printf("->%d", iterator->node->key);
				iterator = iterator->next;
			}
			printf("\n");
		}
	}

	printf("\nBFS demo:\n ");
	for (i = 0; i<7; i++)
		if (demo[i]->color == 0)
			BFS(demo, demo[i], 7, 10, '5');
	/*
	//vertices
	nodeG **arr;
	srand(time_t(NULL));
	v = 100;
	for (e=1000; e<=5000; e+=100)
	{
	arr = createGraphNodes(v);
	randomGraphGenerator(arr, v, e);
	for (i=0; i<v; i++)
	if (arr[i]->color == 0)
	{
	cout<<endl;
	BFS(arr, arr[i], v, e, 'e');
	}
	}


	//edges
	e=9000;
	for (v=100; v<=200; v+=10)
	{
	arr = createGraphNodes(v);
	randomGraphGenerator(arr, v, e);
	for (i=0; i<v; i++)
	if (arr[i]->color == 0)
	{
	cout<<endl;
	BFS(arr, arr[i], v, e, 'v');
	}
	}
	profiler.createGroup("Edge Operations", "Operations");
	profiler.createGroup("Node Operations", "Operations");
	profiler.showReport();*/
	return 0;
}