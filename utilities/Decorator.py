'''
Created on Mar 7, 2017

@author: pasca
'''

from timeit import default_timer as time


def timeit(f):

    def timed(*args, **kw):

        ts = time()
        result= f(*args, **kw)
        te = time()
        return [te-ts,result]

    return timed