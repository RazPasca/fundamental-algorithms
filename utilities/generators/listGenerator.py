'''
Created on Mar 7, 2017

@author: pasca
'''
import numpy.random as random
import random as random_uniq
import math

def avg_list(n,min=0,max=1000):
    a = [random.randint(min,max) for _ in range(0,n)]
    return a

def descending_list(n,min=0,max=1000):
    start = 0
    a=[]
    for i in range( n, 0, -1 ):
        start = start + math.log( random.random( ) ) / i
        next=(int)( math.exp( start ) * ( max - min ) + min)
        a.append(next)
    return a

def sorted_list(n,min=0,max=2000):
    a = sorted([random.randint(min,max) for _ in range(n)])
    return a

def uniq_sorted_list(n, min=0, max=2000):
    a = sorted(random_uniq.sample(range(min,max),n))
    return a
