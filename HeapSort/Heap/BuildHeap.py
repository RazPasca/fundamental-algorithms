from HeapSort.Heap.Heapify import heapify
from HeapSort.Heap.siftUp import sift_up
from utilities.Decorator import timeit


def build_heap1(a,n,ass=0,comps=0):
	ops=0
	for i in range(n//2,-1,-1):
		ops=heapify(a,n,i)
	return ops


def build_heap2(a):
	heap=[]
	ops=0
	for i in range (len(a)):
		heap.append(a[i])
		ops=sift_up(heap,len(heap)-1,ops)
		#print (ass,comps)
	return ops