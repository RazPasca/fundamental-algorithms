
def parent(i):
	return (i-1)//2

def sift_up(a,i,ass=0,comps=0):

	ops=0
	#comps+=1
	while i!=0 and a[parent(i)]<a[i]:
		#ops+=4
		ass+=3
		comps+=1
		a[parent(i)],a[i] = a[i],a[parent(i)]
		i=parent(i)
	#print (ass,comps)
	return ass,comps