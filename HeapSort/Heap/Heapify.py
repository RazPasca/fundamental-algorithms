def left(i):
	return 2 * i + 1


def right(i):
	a = 2 * i + 2
	return a


def heapify(a, n, i):
	l = left (i)
	r = right (i)
	largest = i
	ops=0
	if l < n:
		ops+=1
		if a[l] > a[largest]:
			largest = l

	if r < n:
		ops += 1
		if a[r] > a[largest]:
			largest = r

	if largest != i:
		ops+=3
		a[i], a[largest] = a[largest], a[i]
		ops+=heapify (a,n,largest)
	return ops