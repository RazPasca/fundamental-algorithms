from HeapSort.Heap.BuildHeap import build_heap1, build_heap2
from HeapSort.Heap.Heapify import heapify
from utilities.Decorator import timeit
def heap_sort1(a):
	v=a[:]
	n = len(v)
	ops=build_heap1(v,n)
	for i in range (n - 1, 0, -1):
		ops+=3
		v[i], v[0] = v[0], v[i]  # swap
		ops+=heapify (v, i, 0)
	#print("with heapsort",v)
	return ops


def heap_sort2(a):
	v=build_heap2(a)
	t=v[1][0]
	n=len(t)
	for i in range (n-1,0,-1):
		t[i], t[0] = t[0], t[i]  # swap
		heapify (t, i, 0)
	#print ("the sorted array is",t)
	return v[0],v[1][1]

