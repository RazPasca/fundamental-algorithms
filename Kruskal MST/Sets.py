class Edge:
	def __init__(self, start, end, length):
		self.start = start
		self.end = end
		self.length = length

	def __repr__(self):
		return "s={},e={},w={}".format (self.start, self.end, self.length)


	def __eq__(self, other):
		"""Override the default Equals behavior"""
		if isinstance (other, self.__class__):
			return ((self.start == other.start) and (self.end == other.end) or
			        ((self.end==other.start) and (self.start==other.end)))
		return False


	def __ne__(self, other):
		"""Define a non-equality test"""
		return not self.__eq__ (other)


class Set:
	def __init__(self, node):
		self.parent = node
		self.rank = 0

	def __repr__(self):
		return "p={},r={}".format (self.parent, self.rank)


class Graph:
	def __init__(self, V=0):
		self.V = V
		self.E = []
		self.operations = 0

	def addEdge(self, edge):
		self.E.append (edge)

	def find(self, sets, i):
		self.operations+=1
		if sets[i].parent != i:
			self.operations+=1
			sets[i].parent =self.find(sets,sets[i].parent)
		return sets[i].parent

	def union(self,x,y,sets): #union by rank
		x_root= self.find(sets,x)
		y_root= self.find(sets,y)
		self.operations+=3
		if sets[x_root].rank<sets[y_root].rank:
			self.operations+=1
			sets[x_root].parent=y_root
		else:
			self.operations+=2 #count assignment and if condition
			sets[y_root].parent=x_root
			if sets[y_root].rank==sets[x_root].rank: #if the 2 sets have the same rank, increase the final rank by one
				self.operations+=1
				sets[y_root].rank+=1

	#
	# def find(self, parent, x):
	# 	self.operations += 1
	# 	if parent[x] != x:
	# 		self.operations += 1
	# 		parent[x] = self.find (parent, parent[x])
	# 	return parent[x]
	#
	# def union(self, parent, rank, x, y):
	# 	self.operations += 1
	# 	if rank[x] < rank[y]:
	# 		self.operations += 1
	# 		parent[x] = y
	# 	else:
	# 		parent[y] = x
	# 		self.operations += 2
	# 		if rank[x] == rank[y]:
	# 			rank[y] += 1

	def MST(self):
		result = []
		e = 0
		i = 0
		parent = [None] * self.V
		rank = [0] * self.V  # parent and rank form the sets, each one representing a tree Together, we have a forest
		for node in range (self.V):
			# sets.append(Set(node))
			parent[node] = node
		self.E = sorted (self.E, key=lambda edge: edge.length)
		while e < self.V - 1:
			edge = self.E[i]
			i += 1
			x = self.find (parent, edge.start)  # the 2 trees that are connected by the edge e
			y = self.find (parent, edge.end)
			self.operations += 1
			if x != y:
				result.append (edge)
				e += 1
				self.union (parent, rank, x, y)
		return result
