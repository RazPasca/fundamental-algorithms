import random

from Kruskal.Sets import Graph, Edge, Set
"""The Kruskal algorithm forms the MST of a connected graph. This approach uses the operations on disjoint sets to achieve this end.
Given the fact that we employ simple data structures for storing the information, the Kruskal algorithm is preffered instead of Prim's
algorithm. The algorithm has 3 steps. Firstly, the initialization of the sets (which, together, represent a forest of trees), takes
O(V) time, where V is the number of vertices. Secondly, the edges are sorted according to their weight. For this, we employ the
already implemented sorting procedure from python, which employs a O(ElogE) strategy in the worst case. Finally, we come to the
actual operations on sets. We take the edges in nondecreasing order of their weights, and check the sets from where the 2 end points
belong. If the 2 vertices are in different sets (i.e. they do not form a cycle when added), we add them to the array which stores
the resulting Tree. We do this until we check V-1 edges, obeying the requirements for a tree. We employ the path compression and
union by rank heuristics for the algorithms on disjoint sets, which keep the height of the tree representing the set to a minimum.
This results in a O(VlogV) complexity for the 3rd step. Finally, we get O(V+ElogE+VlogV) which means O(ElogV) or O(ElogE),
 taking into account that |E|<|V|^2. """

def test(edges, V,f):
	g = Graph (V)
	g.E=edges
	mst=g.MST()
	print(g.operations)
	f.write("{},{}\n".format(V,g.operations))

def proof():
	sets=[]
	for i in range(11):
		sets.append (Set(i))
	print(sets)
	g=Graph()
	g.union(1,2,sets)
	for i in range(0,5,2):
		g.union(i+1,i+2,sets)
		print(i,g.find(sets,i))
		print(sets)
	for i in range(6,10):
		g.union(i,i+1,sets)
		print(i,g.find(sets,i))
		print(sets)


def main():
	f = open ("kruskal_ops.csv", "w")
	f.write ("nr of vertices,nr ops\n")
	for V in range(100,10001,100):
		edges=[]
		i=0
		for i in range(V-1):
			edge=Edge(i,i+1,random.randrange(1,2*V))
			edges.append(edge)
		while i<3*V:
			edge= Edge(random.randrange(0,V),random.randrange(0,V),random.randrange(1,2*V))
			if edge not in edges:
				edges.append(edge)
				i+=1
		test(edges,V,f)
	f.close()

if __name__ == '__main__':
	proof()