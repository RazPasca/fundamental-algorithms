import random

from Kruskal.testi import Graph

def test(edges, V, f):
	g = Graph (V)
	for edge in edges:
		g.addEdge (edge)
	# print(g.E)
	mst = g.MST ()
	# print(mst)
	print (g.operations)
	f.write ("{},{}\n".format (V, g.operations))


def main():
	f = open ("kruskal.csv", "w")
	f.write ("nr of vertices,nr ops\n")
	# V=4
	# edges=[Edge(0,1,10),Edge(0,2,6),Edge(0,3,5),Edge(1,3,15),Edge(2,3,4)]
	# test(edges,V,f)

	for V in range (100, 10001, 100):
		edges = []
		# random.seed
		i = 0
		while i < 4 * V:
			edge = [random.randrange (0, V), random.randrange (0, V), random.randrange (1, V * 5)]
			if edge not in edges:
				edges.append (edge)
				i += 1
		test (edges, V, f)
	f .close ()

if __name__ =='__main1__':



