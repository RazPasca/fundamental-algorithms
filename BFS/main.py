import random
from tsl2561 import TSL2561
import RPi.GPIO as GPIO
from BFS.graph import Graph

"""The Breadth-first search (BFS) is an algorithm used to discover all the vertices in a graph connected to the one from
which the search starts. On the way to completion, the algorithm visits all the vertices of the current node, and only after
that passes to the next vertex found in queue. It uses the adjacency list representation, which ocupies O(E+V) space in
memory, making it more efficient for sparse graphs than the matrix representation. Given its versatility, it can be easily
adapted for other problems such as finidng the shortest path in a graph. The overall complexity is O(E+V). The goal of the
algorithm is to visit every vertex exactly once, therefore the V in the formula is explained. The number of edges checked
depend on the type of the graph. In our case, since we have a directed graph, every edge is examined at most once. For
undirected ones, an edge is taken into consideration for both vertices.However, this constant vanishes when taking the
 asymptotic behavior. Also, for a acyclic graph, E=V, while for a complete graph, we have ~V^2 edges. Thus, the complexity
  varies between large margins, from a linear time for sparse/acyclic graphs to a quadratic one if we have a complete graph"""


def proof():
	g = Graph (7)
	g.add_edge (0, 1)
	g.add_edge (0, 2)
	g.add_edge (1, 2)
	g.add_edge (2, 0)
	g.add_edge (2, 3)
	g.add_edge (3, 3)
	g.add_edge (4, 5)
	g.add_edge (6, 5)
	g.add_edge( 6,4)
	g.BFS_all ()

def test_E_variation(V, E, f):
	g = Graph (V)
	edges_added = 0
	while edges_added < E:
		start = random.randrange (0, V)
		end = random.randrange (0, V)
		if g.add_edge (start, end):
			edges_added += 1
	g.BFS_all ()
	# print(len(g.node_list))
	# print(edges_added)

	f.write ("{},{},{}\n".format (V, E, g.ops))


def test_V_variation(V, E, f):
	g = Graph (V)
	edges_added = 0
	while edges_added < E:
		start = random.randrange (0, V)
		end = random.randrange (0, V)
		if g.add_edge (start, end):
			edges_added += 1
	g.BFS_all ()
	# print(len(g.node_list))
	# print (edges_added)
	f.write ("{},{},{}\n".format (V, E, g.ops))


def main():
	f = open ("E variantion.csv", "w")
	f.write ("V,E,ops\n")
	for E in range (1000, 5001, 100):
		test_E_variation (100, E, f)

	f1 = open ("V variation.csv", "w")
	f1.write ("V,E,ops\n")
	for V in range (100, 201, 10):
		test_V_variation (V, 9000, f1)


if __name__ == '__main__':
	proof()