import sys
from collections import deque

from TreeRepresentations.NodeClasses import MultiTreeNode


class Node:
	def __init__(self, key):
		self.key = key
		self.color = "white"
		self.parent = -1
		self.dist = sys.maxsize
		self.neighbors = {}

	def addNeighbor(self, nbr):
		self.neighbors[nbr]= 1

	def __repr__(self):
		if self != None:
			return "key={},color={},parent={}".format(self.key,self.color,self.parent)


class Graph:
	def __init__(self, V):
		self.V = V

		self.node_list = {}
		for i in range(V):
			self.add_node(i)
		self.ops = 0
		self.roots=[]

	def add_node(self, key):
		new_node = Node (key)
		self.node_list[key] = new_node
		#self.ops += 3

	def add_edge(self, start, end):
		# we add the end points if they are not already added
		# if start not in self.node_list:
		# 	self.add_node (start)
		# if end not in self.node_list:
		# 	self.add_node (end)
		if self.node_list[end] not in self.node_list[start].neighbors and end!=start: #if the edge doesn't exist, we add it
			self.node_list[start].addNeighbor (self.node_list[end])
			return True
		else:
			return False #else we return false

	def BFS(self, start_node):
		start_node.dist = 0
		start_node.color = "gray"
		self.ops += 3 #color,dist and append
		node_queue = deque ()
		node_queue.append (start_node)
		tree_nodes=[]
		while node_queue:
			working_node = node_queue.popleft ()
			self.ops+=1
			for nbr in working_node.neighbors:
				self.ops += 1
				if nbr.color == "white":
					nbr.color = "gray"
					nbr.dist = working_node.dist + 1
					nbr.parent = working_node.key
					node_queue.append (nbr)
					self.ops += 4 #color,dist,parent,append

			working_node.color = "black"
			tree_nodes.append(working_node)
			self.ops += 1
		self.roots.append(start_node)
		self.transform_to_multiway(tree_nodes)


	def BFS_all(self):
		for _,node in self.node_list.items():
			node.color = "white"
			node.parent = -1
			node.dist = sys.maxsize
			self.ops += 3
		for _,node in self.node_list.items():
			self.ops+=1
			if node.color == "white":
				self.BFS(node)

	def transform_to_multiway(self,tree_nodes):
		n= self.V+1
		multiway_list = [None for _ in range (0, n + 1)]

		root = [None]
		for node in tree_nodes:
			create_multi_node (tree_nodes, node, multiway_list, root)
		self.print_multi(root[0])


	def print_multi(self,root,spaces=0):
		if root==None:
			return
		printer="  "*spaces
		print(printer,root.key)
		if len(root.children)>=1:
			self.print_multi(root.children[0],spaces+1)
		if len(root.children)>1:
			[self.print_multi(node,spaces+1) for node in root.children[1:]]


def create_multi_node(node_list, node, created_nodes, root):
	# If this node is already created
	if created_nodes[node.key]is not None:
		return

	# Create a new node and update the created_nodes vector
	created_nodes[node.key] = MultiTreeNode (node.key)

	if node.parent == -1: #assign the root if necessary
		root[0] = created_nodes[node.key]
		return

	# If parent is not created, then create parent first
	if created_nodes[node.parent] is None:
		create_multi_node (node_list, node_list[node.parent], created_nodes, root)

	# Find parent pointer and update the children
	p = created_nodes[node.parent]
	p.children.append(created_nodes[node.key])