class HashTable:
	c1 = 1
	c2 = 1

	def __init__(self):
		self.list = [None] * 10007
		self.alpha = 0
		self.items = 0
		self.ops_found = 0
		self.max_ops_found = 0
		self.ops_nfound = 0
		self.max_ops_nfound = 0

	def hashFunction(self, x, i):
		return (x % 10007 + self.c1 * i + self.c2 * i ** 2) % 10007

	def updateAlpha(self):
		self.alpha = self.items / 10007

	def insert(self, x):
		for i in range (0, 10008):
			pos = self.hashFunction (x, i)
			if self.list[pos] == None:
				self.list[pos] = x
				self.items += 1
				self.updateAlpha ()
				return True
		return False

	def search(self, x):
		ops = 0
		i=0
		pos = self.hashFunction (x, i)
		ops+=1
		while i<10008 and self.list[pos] != None: # we stop when we encounter nill space
			ops += 1
			if self.list[pos] != None and self.list[pos] == x: # if element is found, update values and return true
				self.ops_found += ops
				if self.max_ops_found < ops:
					self.max_ops_found = ops
				return True
			i += 1
			pos = self.hashFunction (x, i)

		self.ops_nfound += ops
		if self.max_ops_nfound < ops:
			self.max_ops_nfound = ops
		return False
