import random

from HashTable.hashTable import HashTable
""" Hash Tables are structures which use a function which hashes a set of values of one type(of arbitrary size) to a set of known dimension,
  significantly smaller than the previous one. In the process, this also mixes up the values. All kinds of types can be mapped, and their
  original identifier is reduced to a integer in range (0, m-1). Open adressing hashtables avoid the use of linked lists for storing elements
  with the same hash-values, by probing or by double hashing. Among probing, quadratic probing has some advantages over linear probing. since
  it avoids clustering of values and preserving locality. It's average case performance is O(1), beating data structures such as BSTs or Heaps
  when it comes to searching, inserting and so on. Therefore, the elements will be more spread in the table, which greatly reduces the
  number of colisions. The performance however, is very dependent on the value of load factor alpha, since the nr of operations required increase
  linearly in the worst case, since more and more locations will be need to be probed in order to find an adequate spot. Moreover, since the jumps
  are quadratic in nature, they sometimes avoid large groups of consecutive occupied spaces. Unfortunately, since our function is a quadratic one,
  its hard to guarantee that all the positions will be checked. In fact, if the size of the table is a prime number, we can only guarantee that
  our function will check m/2 distinct positions, where m is the size of the table. Other combinations of size and constants can provide better
  effects."""

def test(alpha, f):
	ht = HashTable ()
	while ht.alpha < alpha:
		x = random.randint (0, 20000)
		ht.insert (x)
	found_nr = 0
	nfound_nr = 0
	while found_nr < 1500:
		x = random.randint (0, 20000)
		if x in ht.list:
			found_nr += 1
			ht.search (x)
	while nfound_nr < 1500:
		x = random.randint (0, 20000)
		if x not in ht.list:
			nfound_nr += 1
			ht.search (x)

	f.write ("{0},{1},{2},{3},{4}\n".format (alpha, ht.ops_found / 1500, ht.max_ops_found, ht.ops_nfound / 1500,
	                                         ht.max_ops_nfound))

def proof():
	ht = HashTable()
	ht.insert(2)
	ht.insert(1000)
	print(ht.search(2))
	print(ht.search(1000))
	ht.insert(11009)
	print(ht.search(11009))
	print(ht.search(3))
	print(ht.max_ops_found, ht.max_ops_nfound)
# if ht.items<10:
#	print(x)
# for i in ht.list:
# 	existing[i] = existing.get (i, 0) + 1
#
# a = [i for i in range(0, 30000) if i not in ht.list]
# found_nr = 0
# for i, val in existing.items ():
# 	if i != None and val > 0:
# 		found_nr += 1
# 		ht.search (i)
# 		if found_nr > 1500:
# 			break
# notfound_nr = 0
# for i in a:
# 	ht.search (i)
# 	notfound_nr += 1
# 	if notfound_nr > 1500:
# 		break


def main(f):
	for i in range (0, 5):
		test(0.8, f)
	for i in range (0, 5):
		test(0.85, f)
	for i in range (0, 5):
		test (0.90, f)
	for i in range (0, 5):
		test (0.95, f)
	for  i in range (0, 5):
		test (0.99, f)


if __name__ == '__main__':
	#f = open ("operations1.csv", "a")
	#f.write ("alpha, ops found, max ops found, ops notfound, max ops notfound\n")
	#main (f)
	proof()