import random

from MergeKLists.BinHeap import MinHeap
from MergeKLists.Node import Node
from utilities.generators.listGenerator import avg_list, sorted_list
"""This problem can be solved in several ways, however 2 of them are efficient: Using a Divide and Conquer approach by recursivelly merging the
lists, halving the number of lists at each step or by using a Min Priority Queue. The complexity for the 2 is (nlogk) where n is the total number
of elements to be sorted, and k the number of lists.  However, the memory required by the first method is a lot higher (since we need to store
the intermediar lists somewhere) than the second one, which employs a Heap of size k. The heap takes advantage of previous comparisons, by storing
the results such that the father is smaller than both his children. This property is mantained by the heapify procedure. The heap is built using
the sift_up procedure, which allows us to create a dynamically sized list, by taking the first element out of every list. Afterwards, the root is
extracted and replaced with the next element from the list indicated by the list_index field of the root(its list of provenience). Analysing the
graphs, we can remark that our function has 2 parameters:n,k. K contributes logharitmically to the complexity, hence the graph with the nr of
operations as function of k (n constant) has a logarithmic shape. When k is constant 5,10,100 the shape of the function is determined by n. This
somehow resembles heapsort, however, we have the size of the heap=k, which is used to sort n elements, hence nlogk. The difference between the
3 is given by the value of logk, which is aprox 2, 3 and 6 for the above k values.
"""

def test(n,k,f):
	if n!=k:
		t=[random.randint(n//(2*k),n//k) for _ in range(0,k-1)]
		t.append(n-sum(t))
	else:
		t = [1 for _ in range (0, k )]
	a=[sorted_list(t[i]) for i in range(0,k)]
	print(a)
	result=[]
	heap=MinHeap()
	for i in range(0,k):
		t=Node(a[i].pop(0),i)
		heap.push(t)
	for i in range(0,n):
		v,index=heap.pop()
		result.append(v)
		if a[index]:
			heap.add(Node(a[index].pop(0),index))
	print(result)
	#f.write("{0},{1},{2}\n".format(n,k,heap.ops))


	return a
def main():
	f=open("k_variation.csv","a")
	f.write("n,k,operations\n")
	#for i in range(100,10001,100):
		#test(i,5,f)
	#for i in range(100,10001,100):
		#test(i,10,f)
	#for i in range(100,10001,100):
		#test(i,100,f)
	#for i in range(10,501,10):
	test(20,4,f)

if __name__ == '__main__':
	main()