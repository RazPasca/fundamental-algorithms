

class Node:
	def __init__(self,value,list_index):
		self.value=value
		self.list_index=list_index

	def __str__(self):
		return "value {0};list_index {1}".format(self.value,self.list_index)

	def __repr__(self):
		return str(self)