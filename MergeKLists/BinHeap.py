
def parent(i):
	return ( i -1 ) //2

def left(i):
	return 2 * i + 1

def right(i):
	return 2 * i + 2

class MinHeap:
	def __init__(self):
		self.heapList = []
		self.size = -1
		self.ops=0


	def sift_up(self ,i):
		while i>0:
			self.ops+=1
			p=parent(i)
			if self.heapList[p].value > self.heapList[i].value:
				self.ops+=3
				self.heapList[p], self.heapList[i] =self.heapList[i], self.heapList[p]
			i = p


	def push(self,x):
		self.heapList.append(x)
		self.size+=1
		self.sift_up(self.size)


	def heapify(self,i):
		l = left (i)
		r = right (i)
		smallest = i
		if l <= self.size:
			self.ops += 1
			if self.heapList[l].value < self.heapList[smallest].value:
				smallest = l
		if r <= self.size:
			self.ops += 1
			if self.heapList[r].value < self.heapList[smallest].value:
				smallest = r
		if smallest != i:
			self.ops += 3
			self.heapList[i], self.heapList[smallest] = self.heapList[smallest], self.heapList[i]
			self.heapify (smallest)

	def pop(self):
		if self.heapList:
			val=self.heapList[0]
			#self.ops+=1
			#self.heapList[0] = self.heapList[self.size]
			self.size-=1
			self.heapList.pop(0)
			#self.heapify(0)
			return val.value, val.list_index

	def add(self,x):
		self.heapList.insert(0,x)
		self.size+=1
		self.heapify(0)