'''
Created on Mar 7, 2017

@author: pasca
'''

from sortingMethods.Decorator import timeit

@timeit
def insertionSort(a,ass=0,c=0):
    v=list(a)
    for i in range(1,len(v)):
        #curVal is the value to be inserted
        curVal = v[i]
        ass+=1
        pos = i
        c+=1
        while curVal<v[pos-1] and pos>0:
            c+=1
            # the following line shifts the elements of the list if necessary and decrements pos
            v[pos], pos = v[pos-1], pos-1  
            ass+=1
        #finally, insert curVal where it should belong
        v[pos] = curVal
        ass+=1
    return [v,c,ass]
   
        