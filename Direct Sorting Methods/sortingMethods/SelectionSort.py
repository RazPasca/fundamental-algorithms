'''
Created on Mar 5, 2017

@author: pasca
'''

from sortingMethods.Decorator import timeit

@timeit
def selectionSort(a,ass=0,c=0):
    """we use the indexed selection sort 
    since it is faster for swapping"""
    v = list(a)
    for i in range(len(v)):
    # Make a generator expression to return (value, i) pairs.
        genexp = ((n, i) for i, n in enumerate(v[i:]))
    # Use genexp with min() to find lowest and its index.
        _, iMin = min(genexp)
        c+=i-1
    # Adjust index to be correct in full list, since the slice reindexes elements
        iMin += i
        v[i], v[iMin] = v[iMin], v[i]
        ass+=3
    return [v,c,ass]  