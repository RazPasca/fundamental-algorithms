'''
Created on Mar 7, 2017

@author: pasca
'''
from sortingMethods.Decorator import timeit



def bSearch(a,lo,hi,x,ass=0,c=0):
    """function which returns the index of element x
    in an ordered array"""
    c = c+1
    if lo<=hi:
        ass +=1
        mid = lo+(hi-lo)//2
        c+=1
        if a[mid]==x:
            return [mid,c,ass]
        c+=1
        if a[mid]>x:
            return bSearch(a, lo, mid-1, x,ass,c)
        c+=1 
        if a[mid]<x:
            return bSearch(a,mid+1,hi,x,ass,c)
    return [lo,c,ass]

@timeit   
def insertionSort(a,ass=0,c=0):
    """sorts the array a using the insertionSort method
    and the bSearch function"""
    v=a[:]
    for i in range(1,len(v)):
        pos=bSearch(v,0,i,v[i],ass,c)
        ass=pos[1]
        c=pos[2]
        ass+=1
        if pos!= i:
            ass+=1
            aux=v[i]
            for j in range (i-1,pos[0]-1,-1):
                ass+=1
                v[j+1]=v[j]
            v[pos[0]]=aux
            ass+=1
    return [v,c,ass]
        
        