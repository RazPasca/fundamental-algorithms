'''
Created on Mar 5, 2017

@author: pasca
'''

from sortingMethods.Decorator import timeit

@timeit
def bubbleSort(a,ass=0,c=0):
    v=a[:]
    change=True
    iterations=len(a)-1
    while iterations>0 and change:
        change=False
        for i in range(iterations):
            c+=1
            if v[i]>v[i+1]:
                ass+=3
                v[i],v[i+1]= v[i+1],v[i]
                change=True
            
        iterations=iterations-1
    return [v,c,ass]        