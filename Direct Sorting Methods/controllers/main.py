'''
Created on Mar 5, 2017

@author: pasca
'''

from sortingMethods.SelectionSort import selectionSort
from sortingMethods.BubbleSort import bubbleSort
from sortingMethods.LinearInsertion import insertionSort
from sortingMethods.BSInsertion import insertionSort as bInsertionSort
from generators.listGenerator import worstList, bestList, avgList

def main():
    '''Those 3 sorting algorithms present a O(n^2) time complexity. However, there are sensible differences when talking about
    worst, best and avg cases. The SelectionSort has no particular cases, its number of operations (assignments are linear,
    comparisons are ~ n^2/2) is the same. This means that on a descended sorted array, it performs better than the others.
    On a ascended sorted array, it performs the worst. Bubble sort is the worst sort time wise, which can be concluded by lookng
    at the average case. Insertion sort, even if outdone by Selection sort on the worst case, on the average case performs 
    half the number of comparisons of selection sort. Their respective number of operations is equal since Selection Sort 
    has a linear dependency of assignments. This means that for uses where the nr of assignments is costly(writing a memory)
    Selection should be favoured instead of Insertion Sort. If the array tends to be sorted, Insertion Sort will outperform it.
    If we take into account the BS Insertion sort, and compare the timings on the average case we have the following numbers:
    BS Insertion:3.4051, Regular Insertion: 8.8194, Bubble Sort: 13.8971, Selection Sort:8.8994. We have a clear winner here, 
    with BS Insertion having also half the number of operations of Regular Insertion or Selection Sort.
    Selection sort is not stable, while the others two are stable, preserving the relative order of equal elements.
    These result have been obtained by using the timer from the Decorator module. More results are found in the files from the 
    controllers package.'''
    '''
    a = [[]for _ in range (0, 7)]
    a[0]=worstList(10000)
    a[1] = bestList(10000)
    a[2]=avgList(10000)
    a[3]=avgList(10000)
    a[4]=avgList(10000)
    a[5]=avgList(10000)
    a[6]=avgList(10000)
    f1=open('worstcase.csv','w')
    f2=open('bestcase.csv','w')
    f3 = open('avgcase.csv', 'w')
    f3.write("input size,insertion comps,selection comps,bubble comps,insertion assignments,selection assignments,bubble assignments,insertion operations,selection operations,bubble operations\n")
    myInt=5
    for i in range (100, 10000, 200):
        result2=[0,0,0,0]
        result3=[0,0,0,0]
        result4=[0,0,0,0]
        for j in range(2, 7):
            result2i = insertionSort(a[j][:i])
            result2i.append(result2i[1] + result2i[2])
            result3i = bubbleSort(a[j][:i])
            result3i.append(result3i[1] + result3i[2])
            result4i = selectionSort(a[j][:i])
            result4i.append(result4i[1] + result4i[2])
            for k in range (1,4):
                result2[k]=result2[k]+result2i[k]
                result3[k]=result3[k]+result3i[k]
                result4[k]=result4[k]+result4i[k]
        result2[:] = [x // myInt for x in result2]
        result3[:] = [x // myInt for x in result3]
        result4[:] = [x // myInt for x in result4]
        result2.append(i)
        f3.write("{0[4]},{0[1]},{2[1]},{1[1]},{0[2]},{2[2]},{1[2]},{0[3]},{2[3]},{1[3]}\n".format(result2, result3, result4))

    f2.write("input size,insertion comps,selection comps,bubble comps,insertion assignments,selection assignments,bubble assignments,insertion operations,selection operations,bubble operations\n")
    for i in range (100,10000,200):
        result1= bInsertionSort(a[1][:i])
        result1.append(result1[1]+result1[2])
        result1.append(i)
        result2= insertionSort(a[1][:i])
        result2.append(result2[1]+result2[2])
        result2.append(i)
        result3=bubbleSort(a[1][:i])
        result3.append(result3[1]+result3[2])
        result3.append(i)
        result4=selectionSort(a[1][:i]) 
        result4.append(result4[1]+result4[2])
        result4.append(i)                  
        f2.write("{0[4]},{0[1]},{2[1]},{1[1]},{0[2]},{2[2]},{1[2]},{0[3]},{2[3]},{1[3]}\n".format(result2, result3, result4))
    
    f1.write("input size,insertion comps,selection comps,bubble comps,insertion assignments,selection assignments,bubble assignments,insertion operations,selection operations,bubble operations\n")
    for i in range(100,10000,200):
        result1= bInsertionSort(a[j][:i])
        result1.append(result1[1]+result1[2])
        result1.append(i)
        result2= insertionSort(a[j][:i])
        result2.append(result2[1]+result2[2])
        result2.append(i)
        result3=bubbleSort(a[j][:i])
        result3.append(result3[1]+result3[2])
        result3.append(i)
        result4=selectionSort(a[j][:i]) 
        result4.append(result4[1]+result4[2])
        result4.append(i)                  
        f3.write("{0[0]:.4f} {0[1]} {0[2]} {0[3]} {0[4]}".format(result4)+" {}\n".format(i))    
    '''



if __name__ == '__main__':
    main()
    a=avgList(15, 0, 10)
    print(a)
    r1=bInsertionSort(a)
    print(r1[0])
    r2=insertionSort(a)
    print(r2[0])
    r3=bubbleSort(a)
    print(r3[0])
    r4=selectionSort(a)
    print(r4[0])

